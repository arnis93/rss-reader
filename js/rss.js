
/*---------------------------------*/
//	get all rss from localStorage
/*---------------------------------*/
function address(){
	var x = document.getElementById("mySelect");

	var url = JSON.parse(localStorage.getItem('history'));

	var count = Object.keys(url).length;

	     for (  var i = 0; i < count; i++ ) {

	var option = document.createElement("option");

	option.text = url[i];

	x.add(option);

	}
 }


/*---------------------------------*/
//	open rss from localstorage
/*---------------------------------*/

function localrss(){

	var lastrsss = localStorage.getItem('lastrss');

	parse(lastrsss);
}

/*---------------------------------*/
//	select rss
/*---------------------------------*/
 
function select(x){

	var urlObj = document.getElementById(x);

	var rssUrl = urlObj.value;

	parse(rssUrl, x);

}

/*---------------------------------*/
//	submit rss
/*---------------------------------*/
 
function submit(x){

	var urlObj = document.getElementById(x);

	var rssUrl = urlObj.value;
	
	parse(rssUrl, x);

	document.getElementById("urlForm").reset();
}


/*---------------------------------*/
//	parse RSS
/*---------------------------------*/

function parse(rssUrl, x){

	var feedtitle = "";
	var html = "";
	var modalhtml = "";
	var input = x;

	if(rssUrl == "" ){
		$("div#title").html(feedtitle);
		$("div#target").html(html);
		$("div#modal").html(modalhtml);
	    	return;
	}

	function parseRSS(url, callback) {
		$.ajax({
			url: 'https://api.rss2json.com/v1/api.json?rss_url=' + encodeURIComponent(rssUrl),
			dataType: 'json',
			success: function(data) {
			callback(data);
			}
		});

	}

	parseRSS(rssUrl, function(rss) {

	 	var status = rss.status;
	    if (status !== "ok" ) {  
	   		alert("Netinkamas feed url!");

    $("div#title").html(feedtitle);
	$("div#target").html(html);
	$("div#modal").html(modalhtml);
	    	return;
	    	} 

	    var feed = rss.feed;

	    feedtitle = "<h2 class='title'>"+feed.title+"</h2>";

		var items = rss.items;
		for(i = items.length-1; i >=0; i--) {

	        html += "<button onclick='modal(this.id)' id='"+ i + "' title='"+ items[i].title +"' class='btn btn-full' datetime='"+ items[i].pubDate +"'><p class='text'>" + items[i].title + 
	        "</p><p class='subtext'>"+ items[i].pubDate +"</p></button>";  

	        modalhtml += "<div id='modal-"+ i + "' class='modal'><div class='modal-content'><div id='content'><h3 class='modal-title'>"+items[i].title+"</h3><p class='inner-text'>"+
	        items[i].description.replace(/<[^>]+>/g, '')+"</p></div><div id='buttons'><button id='btn-link' class='btn btn-full'><a href='"+ 
	        items[i].link +"' target='_blank'>Open</a></button><button id='close-"+i+"' class='close btn btn-full'>Close</button></div></div></div>";  
	        };

	$("div#title").html(feedtitle);
	$("div#target").html(html);
	$("div#modal").html(modalhtml);

	localStorage.setItem('lastrss', rssUrl);


	if(input == "url"){

		var x = document.getElementById("mySelect");
		var option = document.createElement("option");
		option.text = rssUrl;

		for (  var i = 0; i < x.length ; ) {
			if (x[i].value !== option.value ) {  
				i++;
			    if(i == x.length){  
			        x.add(option);
			        break;
			        }
			    }
			    else{   
			       alert("Toks feed url jau yra!");  
			        break;
			    }
		}

		if(x.length == 0){
			x.add(option);
		}

		var obj = {};

		function getSelectOptions(id){

			var select = document.getElementById('mySelect');

			for(var i=0; i< select.options.length; i++){
			    var option = select.options[i];
			    obj[option.value] = i;
			}
			return obj;    
		}

		var opts = getSelectOptions('mySelect');

		function swap(json){
			var ret = {};
			for(var key in json){
				ret[json[key]] = key;
			}
			return ret;
		}

		var b = {};
		b = swap(opts);
		for(var i = 0; i < Object.keys(b).length; i++ ){
		}
		var optss = JSON.stringify(b);

		localStorage.setItem('history', optss);

	}
	});
}




/*---------------------------------*/
//	rss history
/*---------------------------------*/

function history(){

    var history = document.getElementById('myForm');

    if (history.style.display === 'none') {
        history.style.display = 'block';
       
    } else {
        history.style.display = 'none';
    }
 }


/*---------------------------------*/
//	remove rss from list
/*---------------------------------*/

function remove() {

	var x = document.getElementById("mySelect");

	x.remove(x.selectedIndex);

	var url = x.value;
	var obj = {};
	function getSelectOptions(id){

		var select = document.getElementById('mySelect');

		for(var i=0; i< select.options.length; i++){
		    var option = select.options[i];
		    obj[option.value] = i;
		}
		return obj;    
	}

	var opts = getSelectOptions('mySelect');

	function swap(json){

		var ret = {};
		for(var key in json){
		ret[json[key]] = key;
		}
		return ret;
	}

	var b = {};
	b = swap(opts);

	for(var i = 0; i < Object.keys(b).length; i++ ){
	}

	var optss = JSON.stringify(b);

	var empty = "";

	localStorage.setItem('lastrss', empty);

	localStorage.setItem('history', optss);

	parse(url);
 
}

/*---------------------------------*/
//	sort feeds by name
/*---------------------------------*/

function sortName(){

	var toSort = document.getElementById('target').children;

	toSort = Array.prototype.slice.call(toSort, 0);

	toSort.sort(function(a, b) {

	    var aa = a.title.toLowerCase();
	    var bb = b.title.toLowerCase();
	    return (aa > bb) ? 1 : -1;

	});

	var parent = document.getElementById('target');
	parent.innerHTML = "";

	for(var i = 0, l = toSort.length; i < l; i++) {

	    parent.appendChild(toSort[i]);
	}
}



/*---------------------------------*/
//	sort feeds by date
/*---------------------------------*/

function sortDate(){

	var toSort = document.getElementById('target').children;

	toSort = Array.prototype.slice.call(toSort, 0);

	toSort.sort(function(a, b) {

	    var aa = a.attributes.datetime.value;
	    var bb = b.attributes.datetime.value;
	    return (aa > bb) ? 1 : -1;

	});

	var parent = document.getElementById('target');
	parent.innerHTML = "";

	for(var i = 0, l = toSort.length; i < l; i++) {

	    parent.appendChild(toSort[i]);
	}
}



/*---------------------------------*/
//	open modal box
/*---------------------------------*/

function modal(clicked_id){

var modal = document.getElementById("modal-"+clicked_id);
var btn = document.getElementById(clicked_id);
var span = document.getElementById("close-"+clicked_id);

    modal.style.display = "block";

span.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";

    }
}
}	